/** Basic arithmetic operations. */
const mylib = {
    /** Multiline arrow function. */ 
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },

    ZeroDivision: (dividend, divisor) => dividend / divisor,
    
    /** Singleline arrow function. */
    divide: (dividend, divisor) => dividend / divisor,  

    /** Regular function. */ 
    multiply: function(a, b) {
        return a * b; 
    }
};

module.exports = mylib;