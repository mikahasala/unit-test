const expect = require("chai").expect;
const mylib = require("../src/mylib");

describe("Our first unit tests", () => {
    before (() => {
        // initialization
        // create objects... etc..
        console.log("Initialising tests");
    });
    it("Can add 1 and 2 together", () => {
        // Tests
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    it("Can subtract 1 and 2", () => {
        // Tests
        expect(mylib.subtract(1,2)).equal(-1, "1 - 2 is not -1, for some reason?");
    });
    it("Can divide 1 and 2", () => {
        // Tests
        expect(mylib.divide(1,2)).equal(0.5, "1 / 2 is not 0.5, for some reason?");
    });
    it("Can multiply 1 and 2", () => {
        // Tests
        expect(mylib.multiply(1,2)).equal(2, "1 * 2 is not 2, for some reason?");
    });
    it("Can divide 8 and 0", () => {
        // Tests
        expect(mylib.divide(8,0)).equal(0, "Error. Can't be divided by 0");
    });

    after(() => {
        // Cleanup
        // For example: shutdown the Express server.
        console.log("Testing completed!")
    })
});